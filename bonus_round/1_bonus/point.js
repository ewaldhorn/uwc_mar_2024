export class Point {

  constructor(width, height) {
    this.x = 5 + Math.random() * (width * 0.95);
    this.y = 5 + Math.random() * (height * 0.95);
    this.xDirection = Math.random() > 5 ? -1 : 1;
    this.yDirection = Math.random() > 5 ? 1 : -1;
    this.colour = `hsla(${Math.random() * 360}, 100%, 70%, ${Math.random()})`;
  }

  //=======================================================================
  render(ctx) {
    ctx.fillStyle = this.colour;
    ctx.fillRect(this.x-2, this.y-2, 4, 4);
  }

  //=======================================================================
  update(minX, minY, maxX, maxY) {
    if ((this.x <= minX) || (this.x >= maxX)) {
        this.xDirection *= -1;
    }

    if ((this.y <= minY) || (this.y >= maxY)) {
        this.yDirection *= -1;
    }

    this.x += this.xDirection;
    this.y += this.yDirection;
  }
}
