import { Point } from "./point.js";

const POINT_COUNT = 20;

let canvas, context, width, height, points;

//=======================================================================
const initPoints = () => {
  points = [];

  for (let i = 0; i < POINT_COUNT; i++) {
    points.push(new Point(width, height));
  }
};

//=======================================================================
const handleResize = () => {
  width = canvas.width = window.innerWidth * 0.95;
  height = canvas.height = window.innerHeight * 0.9;
};

//=======================================================================
const render = () => {
  context.clearRect(0, 0, width, height);

  context.beginPath();
  context.lineWidth = 1;
  context.strokeStyle = points[0].colour;
  context.moveTo(points[0].x, points[0].y);
    
  for (let i = 0; i < points.length; i++) {
    context.lineTo(points[i].x, points[i].y);
    context.stroke();
    context.strokeStyle = points[i].colour;
    points[i].render(context);
  }

  context.closePath();
  context.stroke();
};

//=======================================================================
const update = () => {
  points.forEach((point) => point.update(5, 5, width - 5, height - 5));
};

//=======================================================================
const animate = () => {
  window.requestAnimationFrame(() => {
    render();
    update();
    animate();
  });
};

//=======================================================================
const init = () => {
  canvas = document.getElementById("output");
  canvas.style.backgroundColor = "black";
  context = canvas.getContext("2d");

  handleResize();
  initPoints();
  animate();
};

//=======================================================================
// Check to see if the DOM has finished loading. If it has, init the app.
window.addEventListener("DOMContentLoaded", init);
window.addEventListener("resize", handleResize);
